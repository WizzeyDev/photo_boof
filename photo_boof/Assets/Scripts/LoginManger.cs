﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoginManger : MonoBehaviour
{
    string m_Email, m_Name;

    public TMPro.TMP_InputField m_NameInput, m_EmailInput;
    public TMPro.TextMeshProUGUI m_Text;
    public RawImage m_BCImage;

    public Texture[] m_Images;

    bool isWaitingSendingMail,isDebug = false;
    static LoginManger instance;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    private void Start() {
        m_BCImage.texture = m_Images[0];
        m_Text.text = "לפני הכיף וכדי שנשלח לכם את הסרטון אתם צריכים לבצע רישום קצר, יאללה לחצו על כפתור המשך.";
    }

    private void Update() {
        if (isDebug) {
            Next();
        }
    }

    private void OnEnable() {
        try {
            if (StagesController.IsEmailBad) {
                if (gameObject.active) {
                    m_BCImage.texture = m_Images[1];
                    m_Text.text = "לצערנו כתובת הדוא''ל שלכם לא נכונה בבקשה הקלידו אותה שוב";
                    m_NameInput.transform.localPosition = new Vector3(m_NameInput.transform.localPosition.x,-154, m_NameInput.transform.localPosition.z);
                    m_EmailInput.transform.localPosition = new Vector3(m_EmailInput.transform.localPosition.x, -339, m_EmailInput.transform.localPosition.z);
                }
            }
            else {
                m_NameInput.transform.localPosition = new Vector3(m_NameInput.transform.localPosition.x, -208, m_NameInput.transform.localPosition.z);
                m_EmailInput.transform.localPosition = new Vector3(m_EmailInput.transform.localPosition.x, -396, m_EmailInput.transform.localPosition.z);
            }
        }
        catch (System.Exception) {

            throw;
        }
       
    }
    public void Next() { //Pressed Button Next
        if (isDebug) {
            m_Name = "Dima Auto Test";
            m_Email = "whizzydev.thed@gmail.com";
            SaveAccount(m_Email, m_Name);
            FindObjectOfType<OnScreenKeyboard>().SetActive(false);
            EmailSender.SendEmail(m_Email, "Welcome to Stars Box", "תודה על השתתפותך בקרוב ישלח אליך הסרטון");
            StagesController.NEXTScreen();
            return;
        }
        m_BCImage.texture = m_Images[0];
        m_Text.gameObject.SetActive(false);
        m_Text.text = "לפני הכיף וכדי שנשלח לכם את הסרטון אתם צריכים לבצע רישום קצר, יאללה לחצו על כפתור המשך.";
        m_Name = m_NameInput.text;
        m_Email = m_EmailInput.text;
        if (m_Email == "" || m_Name == "") {
            m_BCImage.texture = m_Images[2];
            m_Text.text = "הקלידו שם וכתובת דוא''ל רק אז יהיה אפשר להמשיך";
            return;
        }
        if (ValidEmail()) {
            SaveAccount(m_Email, m_Name);         
            FindObjectOfType<OnScreenKeyboard>().SetActive(false);
            EmailSender.SendEmail(m_Email, "Welcome to Stars Box", "תודה על השתתפותך בקרוב ישלח אליך הסרטון");
            StagesController.NEXTScreen();


        }
        else {
            m_BCImage.texture = m_Images[1];
            m_Text.text = "לצערנו כתובת הדוא''ל שלכם לא נכונה בבקשה הקלידו אותה שוב";
        }
    }
    public void Test() {
        //m_NameInput.keyboardType = TouchScreenKeyboardType.EmailAddress;
  
        TouchScreenKeyboard.Open("", TouchScreenKeyboardType.EmailAddress,false,false,true,true);
    }


    public void WroteEmail() {
      
        print(m_EmailInput.text);
        ValidEmail();
    }

    private bool ValidEmail() {
        try {
        var address = new System.Net.Mail.MailAddress(m_EmailInput.text);
       
            return address.Address == m_EmailInput.text;
        }
        catch (System.Exception) {
            m_Text.text = "חייב להזין שם ואימייל לשם המשך";
            throw;
        return false;
        }
    }
 

    private void SaveAccount(string email,string name) {
        PlayerPrefs.SetString("Account", email);
        PlayerPrefs.SetString("Name", name);
    }

    private void DeleteAccount() {
        PlayerPrefs.DeleteKey("Account");
        PlayerPrefs.DeleteKey("Name");
    }
    public static void SetAccount(string email, string name) {
        instance.m_Email = email;
        instance.m_Name = name;
    }
    public static void ClearAllText() {
        try {
        instance.m_NameInput.text = "";
        instance.m_EmailInput.text = "";
            instance.DeleteAccount();
        }
        catch (System.Exception) {

            throw;
        }
    }

    public static void SetLoginImages(Texture[] images) {
        instance.m_Images = images;
        instance.Start();
    }
}

[System.Serializable]
public class ClientInfo {
    public string clientname, emailAdress, captureVideoPath;

    public ClientInfo(string name, string email, string path) {
        emailAdress = email;
        captureVideoPath = path;
        clientname = name;
    }
}
