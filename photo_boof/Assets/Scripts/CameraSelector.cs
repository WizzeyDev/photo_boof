﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSelector : MonoBehaviour {
    #region Singleton
    private static CameraSelector instance;
    private void Awake() {
        if (instance == null)
            instance = this;
        else {
            DestroyImmediate(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    public GameObject devicePrefab;
    public Transform devicesListTransform;
    #endregion

    private GameObject cameraSelectorObject;

    private Text textComponent;

    private bool isOpenSelector;
    private bool isOpenList;

    private void Start() {
        cameraSelectorObject = transform.parent.gameObject;
        textComponent = GetComponentInChildren<Text>();
        cameraSelectorObject.SetActive(false);

    }
    public static void LoadDevicesNames(WebCamDevice[] devices) {
        for (int i = 0; i < devices.Length; ++i) {
            string deviceName = devices[i].name;
            GameObject deviceObj = Instantiate(instance.devicePrefab, instance.devicesListTransform);
            Text textComponent = deviceObj.GetComponentInChildren<Text>();
            textComponent.text = deviceName;
            Button btn = deviceObj.GetComponent<Button>();
            btn.onClick.AddListener(() => {
                instance.ClickOnDivece(deviceName);
            });
        }
    }

    private void ClickOnDivece(string deviceName) {
        WebCam.SelectNewCamera(deviceName);
        CloseDeviceList();
        textComponent.text = deviceName;
        instance.isOpenList = false;
    }

    public static void SetVisibleCameraSelector() {
        if (instance.isOpenSelector) {
            instance.cameraSelectorObject.SetActive(false);
            instance.CloseDeviceList();
            instance.isOpenList = false;
        }
        else
            instance.cameraSelectorObject.SetActive(true);

        instance.isOpenSelector = !instance.isOpenSelector;
        instance.cameraSelectorObject.SetActive(instance.isOpenSelector);
    }

    public void SetOpenDeviceList() {
        if (instance.isOpenList) {
            CloseDeviceList();
        }
        else {
            OpenDeviceList();
        }

        instance.isOpenList = !instance.isOpenList;
    }

    private void OpenDeviceList() {
        devicesListTransform.gameObject.SetActive(true);
    }

    private void CloseDeviceList() {
        devicesListTransform.gameObject.SetActive(false);
    }

    public static void SetCurrentDisplayName(string deviceName) {
        instance.textComponent.text = deviceName;
    }
}
