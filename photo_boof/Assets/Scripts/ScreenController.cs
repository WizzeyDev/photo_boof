﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenController : MonoBehaviour {
    public Texture[] m_EngMenu,m_HebMenu;
    public RawImage m_ScreenScaverHolder,m_LoginHolder,m_EndScreen;
    bool isEng;

    static ScreenController instance;
    private void Awake() {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        m_ScreenScaverHolder.texture = m_HebMenu[0];
        Texture[] images =new Texture[3];
        for (int i = 0; i < images.Length; i++) {
            images[i] = m_HebMenu[(i + 1)];
        }
        LoginManger.SetLoginImages(images);
        m_EndScreen.texture = m_HebMenu[m_HebMenu.Length - 1];
        //LoginManger.SetLoginImages()
        //Invoke("SetImageHolderImges",2f);
    }

    // Update is called once per frame
    void Update()
    {
    }

    void SetImageHolderImges() {
      
        m_LoginHolder.transform.parent.gameObject.SetActive(true);
        print(m_LoginHolder.transform.parent.name + "Active? " + m_LoginHolder.transform.parent.gameObject.active);
        m_LoginHolder.texture = m_HebMenu[1];
        m_LoginHolder.transform.parent.gameObject.SetActive(false);
        m_EndScreen.transform.parent.gameObject.SetActive(true);
        m_EndScreen.texture = m_HebMenu[m_HebMenu.Length - 1];
        m_EndScreen.transform.parent.gameObject.SetActive(false);
    }

    void ChangeLanguage() {

    }

    public static void SetLanguage(bool isEng) {
        if (instance.isEng != isEng) {
            instance.ChangeLanguage();
        instance.isEng = isEng;
        }

    }
}
