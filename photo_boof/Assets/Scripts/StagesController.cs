﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public enum StageScreen{ ScreenSaver,LoginScreen,AcstionScreen,CameraScreen,EndScreen}
public class StagesController : MonoBehaviour {
    public StageScreen m_CurrentScreen;
    public OnScreenKeyboard m_OSK;
    public GameObject[] m_ScreensGO;
    public GameObject m_mailChecking, m_SettingsButton;
    public TMPro.TextMeshProUGUI m_BugTxt;
    public float maxIdleTime = 60;

    int m_SettingsButtonTouched, m_VideoToPlay = 0;
    float idleTime;
    const float m_SessionDurastion = 300, m_AlertingEnd = 60;
    bool isEmailBad, isVideoFinalized, isSessionInProgress, isVideoManualyChanged, isPlayingMotivation, isEmailResenedNeeded;
    static StagesController instance;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        ChangeScreen();
        m_mailChecking.SetActive(false);

        if (isEmailResenedNeeded = RecoveryEmailSender.IsRecoverFileEmpty()) {
            RecoveryEmailSender.RecoverFromFileSendingList();
        }
        
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            isVideoManualyChanged = true;
            m_VideoToPlay = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            isVideoManualyChanged = true;
            m_VideoToPlay = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            isVideoManualyChanged = true;
            m_VideoToPlay = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4)) {
            //isVideoManualyChanged = true;
            //m_VideoToPlay = 3;
            //print("Screen Width saved " + PlayerPrefs.GetInt("ScreenWidth") + " Video To play: " + PlayerPrefs.GetInt(("VideosToPlay")));
        }
        if (Input.GetKeyDown(KeyCode.Alpha5)) {
            isVideoManualyChanged = true;
            m_VideoToPlay = 4;
        }

        if (m_CurrentScreen == StageScreen.ScreenSaver) {
            if (!isPlayingMotivation && Time.timeSinceLevelLoad > idleTime + maxIdleTime && Opstions.ShouldChangeVideo()) {
                isPlayingMotivation = true;
                Opstions.VideomMotivation();
            }
            if (!isPlayingMotivation && isEmailResenedNeeded && !RecoveryEmailSender.IsEmailing() && Application.internetReachability != NetworkReachability.NotReachable) {
                if (!RecoveryEmailSender.IsCliendDataRecovered()) {
                    RecoveryEmailSender.RecoverFromFileSendingList();
                }
                else {

                RecoveryEmailSender.PrepareClientInfoForEmails();
                }
            }
        }
    }

   

    void TurnOffAllScreens() {
        m_OSK.SetActive(false);
        foreach (var item in m_ScreensGO) {
            item.active =false;
        }
    }

    public void ToggleSetting(bool toOpen) {
        m_SettingsButton.SetActive(toOpen);
    }

    IEnumerator  DelayedEmailCheck(float delay) {
        m_mailChecking.SetActive(true);
        yield return new WaitForSeconds(delay);        
        if (isEmailBad = EmailSender.FetchLastMessage()) {
            ChangeScreen();
        }
        else {
            m_CurrentScreen = StageScreen.EndScreen;
            ChangeScreen();
        }
        m_mailChecking.SetActive(false);
    }

    IEnumerator CountTouches(float waitFor,int currentNum) {
        yield return new WaitForSeconds(waitFor);
        if (m_SettingsButtonTouched < 3) {
            m_SettingsButtonTouched = 0;
        }
        yield return null;
    }
    public  void ChangeScreen() {
        TurnOffAllScreens();
        if ((int)m_CurrentScreen > (m_ScreensGO.Length -1)) { //End Session
            isSessionInProgress = false;
            LoginManger.ClearAllText();
            m_CurrentScreen = 0;

        }
            m_ScreensGO[(int)m_CurrentScreen].SetActive(true);
        switch (m_CurrentScreen) {
            case StageScreen.ScreenSaver:
                idleTime = Time.timeSinceLevelLoad;
                break;
            case StageScreen.LoginScreen:
                if (!isSessionInProgress) {
                    isSessionInProgress = true;
                }
                m_OSK.SetActive(true);
                break;
            case StageScreen.AcstionScreen:
                m_CurrentScreen++;
                ChangeScreen();
                break;
            case StageScreen.CameraScreen:
                PhotoBoof.StartPhotoBoof();
                break;
            case StageScreen.EndScreen:          
                break;
            default:
                Debug.Log("Wrong Screen Num" + m_CurrentScreen);
                break;
        }
    }


    public void NextScreen() {
        switch (m_CurrentScreen) {
            case StageScreen.ScreenSaver:
                break;
            case StageScreen.LoginScreen:
                if (isEmailBad) { //
                    StartCoroutine(DelayedEmailCheck(10f));
                    TurnOffAllScreens();
                    return;
                }
               
                break;
            case StageScreen.AcstionScreen:
                break;
            case StageScreen.CameraScreen:
                if (isEmailBad) {
                    m_CurrentScreen = StageScreen.LoginScreen;
                    ChangeScreen();
                    return;
                }
                break;
            case StageScreen.EndScreen:
                if (isEmailBad) {
                    m_CurrentScreen = StageScreen.LoginScreen;
                    ChangeScreen();
                    return;
                }
              
                break;
            default:
                break;
        }

        m_CurrentScreen++;
        ChangeScreen();
    }
    void OpenSettingPasswored() {

    }



    public void OpenSetting() {
        if (m_SettingsButtonTouched == 0) {
        StartCoroutine(CountTouches(3, m_SettingsButtonTouched));
        }
        m_SettingsButtonTouched++;
        if (m_SettingsButtonTouched == 3) {
            if (Opstions.IsPassworedProtected()) {
            Opstions.OpenPasswred();

            }
            else {
                Opstions.ToggleAnimastion(true);

            }
            m_SettingsButtonTouched = 0;
            print("Opening Passwored");
        }
    }

    public void ExitApp() {
        print("Quiteing");
        Application.Quit();
    }


    public static bool StartNewSession() {
        if (!instance.isSessionInProgress) {
            instance.isSessionInProgress = true;
            instance.NextScreen();
            return true;
        }
        else {
            return false;
        }
       
        //TODO: Stop from takeing more money
    }

    public static void NEXTScreen() {
        instance.NextScreen();
    }

    public static void FailedVideoCapture() {
        instance.m_CurrentScreen = StageScreen.AcstionScreen;
        instance.NextScreen();
    }

    public static void DebugText(string txt) {
        instance.m_BugTxt.text = txt;
    }

    public static void SetMotivastion(bool isPLaying) {
        instance.isPlayingMotivation = isPLaying;
        if (!instance.isPlayingMotivation) {
            instance.idleTime = Time.timeSinceLevelLoad;
        }
    }

    public static StageScreen GetScreen { get { return instance.m_CurrentScreen; } }
    #region    ---- Setters & Getters ----
    public static bool IsEmailBad { get { return instance.isEmailBad; } set { instance.isEmailBad = value; } }
    public static bool IsVideoFinalized { get { return instance.isVideoFinalized; } set { instance.isVideoFinalized = value; } }

    public static int GetVideoToPlay { get { return instance.m_VideoToPlay; } }
    public static bool ShouldChangeVideo { get { return instance.isVideoManualyChanged; }set { instance.isVideoManualyChanged = value; } }
    public static GameObject GetScreenGO(int i) {  return instance.m_ScreensGO[i];  }
    public static bool IsReEmailNeeded { get { return instance.isEmailResenedNeeded; } set { instance.isEmailResenedNeeded = value; } }
    #endregion    ---- Setters & Getters ----
}
