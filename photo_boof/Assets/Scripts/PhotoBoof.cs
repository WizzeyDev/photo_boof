﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoBoof : MonoBehaviour {
    public Animator m_CountDown;
    public TMPro.TextMeshProUGUI m_CountDownTxt, m_ExplenastionTxt;
    public RenderHeads.Media.AVProMovieCapture.CaptureBase m_CaptureCtrl;
    public Image m_RecordingIcon;
    public GameObject videoGO;
    public int m_MaxCountDown = 5, m_RandomizeVideo = 0,m_MaxVideosToPlay = 2,m_currentRes;
    UMP.UniversalMediaPlayer m_MediaPlayer;
    WebCam myWebCam;

    public string[] m_VideoPaths;

    bool isRecording, isVideoPrepared, isRandom = false;

    static PhotoBoof instance;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else {
            Destroy(gameObject);
        }
        m_RecordingIcon.gameObject.SetActive(false);
        m_MediaPlayer = GameObject.FindObjectOfType<UMP.UniversalMediaPlayer>();


        for (int i = 0; i < m_VideoPaths.Length; i++) {
            m_VideoPaths[i] = Application.streamingAssetsPath + m_VideoPaths[i];
        }

            m_currentRes = PlayerPrefs.GetInt(("ScreenWidth"));
        print("1 " + m_currentRes);
        if (m_currentRes != 0) {
           
                      print("Player Pref" + m_currentRes + " SizeOf Video " + m_MediaPlayer.RenderingObjects[0].GetComponent<RectTransform>().sizeDelta);

            m_MediaPlayer.RenderingObjects[0].gameObject.SetActive(true);
        m_MediaPlayer.RenderingObjects[0].GetComponent<RectTransform>().sizeDelta = new Vector2(m_currentRes, m_currentRes / 0.5625f);
            m_MediaPlayer.RenderingObjects[0].gameObject.SetActive(false);
            print( " SizeOf Video " + m_MediaPlayer.RenderingObjects[0].GetComponent<RectTransform>().sizeDelta);
        }
        if (PlayerPrefs.GetInt(("VideosToPlay")) != 0) {
            m_MaxVideosToPlay = PlayerPrefs.GetInt(("VideosToPlay")); 
            print("PhotoBoof Saveing Videos "  + m_MaxVideosToPlay);
        }
    }

    private void Start() {
        myWebCam = transform.GetComponentInChildren<WebCam>();
    }
    private void OnEnable() {

        m_MediaPlayer.Stop();     
        m_MaxVideosToPlay = Opstions.GetMaxVideo;
        m_CaptureCtrl._filenamePrefix = PlayerPrefs.GetString("Name"); //Writeing video File with the user Name
        //if (m_MediaPlayer.RenderingObjects[0] != videoGO) {
        //    m_MediaPlayer.RenderingObjects[0] = videoGO;
        //}
        if (myWebCam != null) {
        myWebCam.ToggleWebCam(true);

        }
        else {
            try {
            myWebCam = transform.GetComponentInChildren<WebCam>();
            myWebCam.ToggleWebCam(true);

            }
            catch (System.Exception) {

                throw;
            }
        }
    }


    void StartTutorialCountDown() {
        if (StagesController.ShouldChangeVideo) {
        m_RandomizeVideo = StagesController.GetVideoToPlay;

        }
        if (isRandom) {
            m_RandomizeVideo = Random.Range(0, (m_VideoPaths.Length/2));
            m_RandomizeVideo *= 2; 
        }
        m_MediaPlayer.Path = m_VideoPaths[m_RandomizeVideo];
        m_RecordingIcon.gameObject.SetActive(false);
        StartCoroutine(CountDown());
    }

    void PrepareVideo() {
        isVideoPrepared = false;
        m_MediaPlayer.RenderingObjects[0].GetComponent<UnityEngine.UI.RawImage>().color = new Color(1, 1, 1, 0f);
        m_MediaPlayer.Prepare();
    }

    void PlayVideo() {
        if (!isRecording) {//Start Recording
        m_CaptureCtrl.StartCapture();
        m_MediaPlayer.AddEndReachedEvent(EndVideo);
        m_RecordingIcon.gameObject.SetActive(true);
        m_ExplenastionTxt.gameObject.SetActive(false);
        }
        m_MediaPlayer.RenderingObjects[0].SetActive(true);
        m_MediaPlayer.Play();
        Invoke("FadeIN",0.05f);


    }

    void FadeIN() {
        m_MediaPlayer.RenderingObjects[0].GetComponent<UnityEngine.UI.RawImage>().color = new Color(1, 1, 1, 1);
    }


    public void VideoPrepared() {
        isVideoPrepared = true;

    }

    public void EndVideo() {
        print("End Video");
        m_MediaPlayer.RemoveEndReachedEvent(EndVideo);
           m_MediaPlayer.RenderingObjects[0].GetComponent<UnityEngine.UI.RawImage>().color = Color.clear;
        
        m_MediaPlayer.Play();
        m_MediaPlayer.Position = 0.99f;
        m_MediaPlayer.Stop(false);
  
        //StartCoroutine(VideoEnded());
    }

    IEnumerator CountDown() {
        //new WaitForSeconds(1);
        int x = m_MaxCountDown;
        while (x >= 0) {
            m_CountDownTxt.text = (x).ToString();

            m_CountDown.Play("CountDown", 0, 0);
            x--;
            yield return new WaitForSeconds(1f);
            yield return new WaitUntil(() => m_CountDown.GetCurrentAnimatorStateInfo(0).IsName("Waiting"));
            if (x == 2) {
                PrepareVideo();
            }
        }
        yield return new WaitUntil(() => isVideoPrepared);
        PlayVideo();
        var videoDurastion = (m_MediaPlayer.Length/1000);
        print("Video Lenght" + videoDurastion);
        StartCoroutine(CaptureSequance(videoDurastion+3));
        print("Starting");

    }

    IEnumerator CaptureSequance(float waitFor) {

        print("Capture " + waitFor);
        yield return new WaitForSeconds(waitFor);
        isRecording = false;
        //PlayNextVideo();
        m_RandomizeVideo++;
        if (m_RandomizeVideo >= m_MaxVideosToPlay) { //if Video num above then sent us to end of Captureing
            m_RandomizeVideo = 0;
            print("Reseting Vid " + m_RandomizeVideo);
        }
        m_MediaPlayer.RenderingObjects[0].GetComponent<UnityEngine.UI.RawImage>().color = new Color(1, 1, 1, 0f);
        m_CaptureCtrl.StopCapture();
        yield return new WaitUntil(() => !m_CaptureCtrl.IsCapturing());
        if (!m_CaptureCtrl.IsCapturing()) {
            print(m_CaptureCtrl.LastFilePath);
            StagesController.ShouldChangeVideo = false;
            EndScreen.TurnOnInteractive(m_CaptureCtrl.LastFilePath);
            StagesController.NEXTScreen();
        }
    }

    IEnumerator VideoEnded() {
        yield return new WaitForEndOfFrame();
        //m_MediaPlayer.Pause();


   
    }
    public static void StartPhotoBoof() {

        instance.StartTutorialCountDown();
        //TODO:
    }

    public static void SendEmail(string videoPath) {
   
     
        //TODO:
    }


}
