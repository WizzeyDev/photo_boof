﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Beep : MonoBehaviour
{
    Image m_image;

    // Start is called before the first frame update
    void Start()
    {
        m_image= GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        m_image.color = Color32.Lerp(Color.white, Color.clear, Mathf.PingPong(Time.time, 1));
    }
}
