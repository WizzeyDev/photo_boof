﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;

public static class RecoveryEmailSender 
{
    public static List<string> clienEmails= new List<string>(), clienNames = new List<string>(), clienVideoPath = new List<string>();

    static int emailsToSendPerBatch = 8 ,currentEmailSended = 0,totalEmailsSended = 0;
    static bool  isCliendDataRecovered,isEmailing;

    public static bool IsRecoverFileEmpty() {
        string savedClientInfos = DiskOnKey.ReadString();     
        if (savedClientInfos.Contains("[")) {
            return true;
        }
        else {
            return false;
        }    
    }

    public static void RecoverFromFileSendingList() {
        Debug.Log("Recovered List?");
        if (!isCliendDataRecovered) {
           string savedClientInfos =  DiskOnKey.ReadString();
            char[] be = { '[', ']' };
            var clientInfos = savedClientInfos.Split(be);
            foreach (var emailInfo in clientInfos) {
                if (!string.IsNullOrEmpty(emailInfo)) {
                    if (!emailInfo.Contains("[" + "]") && emailInfo.Contains(";")) {//Filter Unneed strings
                        var split = emailInfo.Trim('[', ']').Split(';');//("[{0};{1};{2}]", name, email, path);
                        clienNames.Add(split[0]);
                        clienEmails.Add(split[1]);
                        clienVideoPath.Add(split[2]);
                    }
                }
            }

                if (clienNames.Count > 0) {
                    isCliendDataRecovered = true;
                }
                Debug.Log("Recovered List count:" + clienNames.Count);
        }
    }

    public static void PrepareClientInfoForEmails() {
        if (clienNames.Count != 0 && currentEmailSended > clienNames.Count-1) { //Stop Sending Emails
            StagesController.IsReEmailNeeded = false;
            //emailsToSendPerBatch = currentEmailSended + emailsToSendPerBatch - clienNames.Count;
            Debug.Log("Changed amount of Emails to send via Array size: " + emailsToSendPerBatch);
            return;
        }
        isEmailing = true;
        Debug.Log("Sending Email " + currentEmailSended + "  Email: "  + clienEmails[currentEmailSended]);
        EmailSender.SendEmailJob(clienEmails[currentEmailSended], clienNames[currentEmailSended], clienVideoPath[currentEmailSended]);
        currentEmailSended++;
    }

    public static void EmailSendedCounter() { //After Emailed Fhinshed Sending
        totalEmailsSended++;
        isEmailing = false;
        if (totalEmailsSended > clienEmails.Count-1) {
            Debug.Log("Fhinshed all the Emails in Data  TODO: Cleanup");
            StagesController.IsReEmailNeeded = false;
            CleanUpData();
        }

        //if (currentEmailSended == emailsToSendPerBatch) { //allow back process to Email more
        //    isEmailing = false;
        //    totalEmailsSended += currentEmailSended;
        //    Debug.Log("Fhinshed A batch  sended now: " + currentEmailSended + " totalEmailsToSend:  "+ totalEmailsSended + " Times Since start " + Time.realtimeSinceStartup);
        //    currentEmailSended = 0;
        //    if (totalEmailsSended + emailsToSendPerBatch == clienEmails.Count) { // Fhinshed all the Emails in Data //TODO: Cleanup
        //        Debug.Log("Fhinshed all the Emails in Data  TODO: Cleanup");
        //        StagesController.IsReEmailNeeded = false;
        //    }
        //}
    }

    public static void SendEmailJob(int emailsToSend) {
        float startTime = UnityEngine.Time.realtimeSinceStartup;     

        //for (int i = 0; i < emailsToSend; i++) {
        //    emails.Add("dimabedsarnat@gmail.com");
        //    names.Add("Test " + i);
        //    videoPath.Add("C:/Repository/photo_boof/photo_boof/Captures/1.mp4");

        //}
        EmailJob job = new EmailJob();
        JobHandle jobHandle = job.Schedule(clienEmails.Count, 100);
        jobHandle.Complete();

        UnityEngine.Debug.Log("Sended Email Job" + ((UnityEngine.Time.realtimeSinceStartup - startTime) * 1000));
    }

    public static void CleanUpData() {
        clienEmails = null;
        clienNames = null;
        clienVideoPath = null;
        isCliendDataRecovered = false;
        DiskOnKey.CleanClientsData();
    }

    public static bool IsEmailing() { return isEmailing; }

    public static bool IsCliendDataRecovered() { return isCliendDataRecovered; }
}
