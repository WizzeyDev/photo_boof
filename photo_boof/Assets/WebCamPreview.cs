﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebCamPreview : MonoBehaviour
{
    private WebCamTexture webCameraTexture;

    private static string deviceName = string.Empty;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<RawImage>();
        //gameObject.AddComponent<RawImage>();
        SelectCamera(deviceName);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void SelectCamera(string cameraDeviceName) {
        var rawImage = GetComponent<RawImage>();
        Vector2 size = GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        if (size == new Vector2(1080, 1920)) { //If portrait 
        
            //size = new Vector2( Screen.height, Screen.width);
            transform.rotation = Quaternion.Euler(transform.rotation.x, 180, 90);
            GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.height, Screen.width);

        }

        webCameraTexture = new WebCamTexture(1080, 1920);
        //Vector2 size = GetComponent<RectTransform>().sizeDelta = new Vector2(1920, 1080);
        rawImage.material.mainTexture = webCameraTexture;
        webCameraTexture.Play();
   
    }
}
