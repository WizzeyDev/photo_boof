﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Opstions : MonoBehaviour {
    public TMPro.TMP_InputField m_MaxVideoToCycle,m_PassworedProtect;
    public TMPro.TextMeshProUGUI m_VideoResoTxt;
    public Slider m_VideoResSlider;
    public Toggle m_EngToggle, m_PassWoredToggle,m_VideoMotivastionToggle;
    public GameObject m_PassworedGO;
    public Image m_connectivityIcon;

    UMP.UniversalMediaPlayer m_MediaPlayer;
    Image m_MyButtonImage;
    Animator m_Anim;
    GameObject m_CalibrateVideo;
    WebCam m_WebCam;
    int m_MaxVideoToPlay,m_MaxResHegih, m_MaxResWidth, m_DefualtRes = 1080,m_currentWidth;
    Vector2 m_VideoRes;
    string videoPath, motivationVideoPath,passwored = "2722";
    bool isRuningCalibrastion, isPassworedProtected = true, shouldChangeVideo = true,isDebug;
    static Opstions instance;
    private void Awake() {
        instance = this;
    }

    void Start() {
        m_Anim = GetComponent<Animator>();
        transform.GetChild(0).gameObject.SetActive(false);
        m_Anim.enabled = false;
        m_MyButtonImage = GetComponent<Image>();
        m_VideoMotivastionToggle.onValueChanged.AddListener(ToggleShouldPlayMotivationVideo);
        m_PassWoredToggle.onValueChanged.AddListener(TogglePassworedProtraction);
        m_PassworedProtect.onValueChanged.AddListener(ValidatePasswored);
        m_MaxVideoToCycle.onEndEdit.AddListener(SetMaxVideoToPlay);
        m_VideoResSlider.onValueChanged.AddListener(SetVideoResolution);
        m_MediaPlayer = GameObject.FindObjectOfType<UMP.UniversalMediaPlayer>();
        m_MaxResWidth = m_DefualtRes * 3;
        m_MaxResHegih = (int)((float)m_MaxResWidth / 0.5625f);
        m_currentWidth = PlayerPrefs.GetInt("ScreenWidth");
        m_MaxVideoToPlay = PlayerPrefs.GetInt("VideosToPlay");
        videoPath = Application.streamingAssetsPath + "/1.mov";
        motivationVideoPath = Application.streamingAssetsPath + "/2.mov";// "/motivation/1.mov";
        //StartVideoCalibrate();
        print("Screen Width saved " + PlayerPrefs.GetInt("ScreenWidth") + " Video To play: "+ PlayerPrefs.GetInt(("VideosToPlay")));
        //InitCalibrateGO();
        //VideomMotivation();
        m_PassworedGO.SetActive(false);
        DiskOnKey.CheckIniFile();
        //gg.AddComponent<>
    }

    public ClientInfo[] clients;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.J)) {
            //VM.WriteToTxt();
            //DiskOnKey.SaveClient()

            //UploadManger.UploadFileDropbox(new byte[2], "DimaTest.txt");

            //for (int i = 0; i < 10; i++) {
            //    DiskOnKey.SaveClient("Test " + i, "whizzydev.thed@gmail.com","C:/Repository/photo_boof/photo_boof/Captures/1.mp4");
            //}
            foreach (var client in clients) {
                //DiskOnKey.SaveClient(client.clientname, client.emailAdress, client.captureVideoPath);
            }
        }
        if (Input.GetKeyDown(KeyCode.D)) {
            //StartCoroutine(UploadFile());

            //UploadManger.UploadFileDropbox(new byte[2], "DimaTest.txt");
      
            //VM.ReadString();
            //print("KA");
            //DiskOnKey.ReadString();
        }
    }

    IEnumerator UploadFile() {
        print("Trying to upload?");
        //Uploading_File = true;
        byte[] data = System.IO.File.ReadAllBytes("C:/Repository/photo_boof/photo_boof/Captures/As@_2020-03-12_12-16-51_540x960.mp4");
        //byte[] data =new byte[2];
        //var request = UploadManger.UploadFileDropbox(data, "dima.mp4");
        var request = UploadManger.GetDropBoxShareLink("dima.mp4");
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone) {
            //LoadingBar.fillAmount = progress + request.uploadProgress / 6.0f;
            yield return null;
        }
        switch (request.responseCode) {
            case 200:
                //UpdateStatus(TargetStatus.File_Uploaded);
                print("Uploaded to DropBox Yay");
                break;
            default:
                // No Internet Connection!
                Debug.LogError("DropBox Error " + request.downloadHandler.text);
        
                //UpdateStatus(TargetStatus.File_Upload_Failed);
                break;
        }
        print(request.responseCode);
        //Uploading_File = false;
    }

    void InitCalibrateGO() {
        m_CalibrateVideo = new GameObject();
        m_CalibrateVideo.transform.parent = transform.root;
        m_CalibrateVideo.transform.SetSiblingIndex(m_CalibrateVideo.transform.parent.childCount - 2);
        m_CalibrateVideo.name = "CalibrateVideo";
        m_CalibrateVideo.transform.localPosition = Vector3.zero;
             
        GameObject webCam = Instantiate(StagesController.GetScreenGO(3).transform.GetChild(0).gameObject, m_CalibrateVideo.transform); //Init a Webcame GO
        m_WebCam = webCam.GetComponent<WebCam>();
        GameObject[] tempVideoArray = new GameObject[2];
        tempVideoArray[0] = m_MediaPlayer.RenderingObjects[0];
        tempVideoArray[1] = Instantiate(m_MediaPlayer.RenderingObjects[0], m_CalibrateVideo.transform);
        m_MediaPlayer.RenderingObjects = tempVideoArray;
        var rawImage = webCam.GetComponent<RawImage>();
        rawImage.raycastTarget = false;

        m_connectivityIcon.color = Application.internetReachability == NetworkReachability.NotReachable ?Color.red :Color.green;


        //m_MediaPlayer.Path = videoPath;
        m_MediaPlayer.RenderingObjects[1].GetComponent<RawImage>().color = new Color(1, 1, 1, 1f);
        m_MediaPlayer.Play();
    }

    void TurnOffCalibrateGO() {
        if (m_CalibrateVideo != null && m_WebCam != null) {
        m_MediaPlayer.Stop();
            m_WebCam.ToggleWebCam(false);
            m_CalibrateVideo.SetActive(false);
        print("Toggled Calibrate Off");
        }

    }



    public void ToggleEnglish() {
        //ScreenController.SetLanguage();
    }

    public void TogglePassworedProtraction(bool isProtected) {
        isPassworedProtected = isProtected;
    }

    public void ToggleShouldPlayMotivationVideo(bool shouldPlay) {
        shouldChangeVideo = shouldPlay ;
        isDebug = shouldPlay;

    }

    public static bool ShouldChangeVideo() {
        return instance.shouldChangeVideo;
    }

    public static bool IsPassworedProtected() {
        return instance.isPassworedProtected;
    }

    public void ExiteApp() {
        print("Exiting?");
        PlayerPrefs.SetInt("ScreenWidth", m_currentWidth);
        PlayerPrefs.SetInt("VideosToPlay", m_MaxVideoToPlay);
        Application.Quit();
    }

    private void OnApplicationQuit() {
        ExiteApp();
    }


    public void SetMaxVideoToPlay(string inputNum) {
        print("SetMaxVideoToPlay " + inputNum);
        m_MaxVideoToPlay = int.Parse(m_MaxVideoToCycle.text);
        //print(int.Parse("MaxVideo: "+m_MaxVideoToCycle.));
    }

    public void SetVideoResolution(float sliderNum) {
     
        if (sliderNum < 0.25f) {
            sliderNum = 0.25f;
        }
        print("Res Slider Touched " );
         m_currentWidth = (int)(m_DefualtRes / sliderNum);
        for (int i = 0; i < m_MediaPlayer.RenderingObjects.Length; i++) { //Change all Video 
        m_MediaPlayer.RenderingObjects[i].GetComponent<RectTransform>().sizeDelta = new Vector2( m_currentWidth,m_currentWidth / 0.5625f);
        }
        m_MediaPlayer.Path = videoPath;
        StartVideoCalibrate();
        m_VideoResoTxt.text = m_currentWidth.ToString();
    }

    public void DefualtVideoRes() {
        SetVideoResolution(0.5f);
    }

    public void HideOpstions() {
        instance.m_Anim.SetBool("SlideIn", false);
    }

    public void StartVideoCalibrate() {
        //Invoke("TurnOffCalibrateGO", 10f);
        if (!isRuningCalibrastion) {
            isRuningCalibrastion = true;
        StopAllCoroutines();
            //StopCoroutine("DelayTurnOffCalibrateGO");
         
        StartCoroutine(DelayTurnOffCalibrateGO((m_MediaPlayer.Length ))); // Shut off the GO after the amount of Video time
        if (StagesController.GetScreen == StageScreen.CameraScreen) {
            return;
        }
        if (m_CalibrateVideo == null) {
            print("INIT?");
            InitCalibrateGO();       
        }
        else {
            print("Working?");
            m_CalibrateVideo.gameObject.SetActive(true);
            m_MediaPlayer.Play();
            m_WebCam.ToggleWebCam(true);         
        }
        }
    }

    public void ValidatePasswored(string pass) {
        if (pass == passwored) {
            ToggleAnimastion(true);
            m_PassworedProtect.text = "";
            m_PassworedGO.SetActive(false);
            print("Passwored Correct");
        }
    }

    public static void OpenPasswred() {
        instance.m_PassworedGO.SetActive(true);
    }

    IEnumerator DelayTurnOffCalibrateGO(float delayTime) {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(m_MediaPlayer.Length/1000);
        TurnOffCalibrateGO();
        StagesController.SetMotivastion(false);
        isRuningCalibrastion = false;
    }

        public void SlidingOut() {
          print("Slide Out");
          m_MyButtonImage.raycastTarget = true;
          gameObject.transform.GetChild(0).gameObject.SetActive(false);
        //TurnOffCalibrateGO();
        }

        public static void ToggleAnimastion(bool isSlideIn) {
            instance.m_Anim.SetBool("SlideIn",isSlideIn);
        if (isSlideIn) {
            instance.m_MyButtonImage.raycastTarget = false;
            instance.m_Anim.enabled = isSlideIn;
        }
        }

    public static int GetMaxVideo {
        get { return instance.m_MaxVideoToPlay; }
    }

    public static void VideomMotivation() {
        instance.m_MediaPlayer.Path = instance.motivationVideoPath;
        instance.StartVideoCalibrate();
    }
}

